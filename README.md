# Dojo de QA - Protractor #

Referências - Livro "Protractor - Lições sobre testes end-to-end automatizados" - Casa do Código - Walmir Filho

### PRÉ-REQUISITOS ###

Node.js - versão superior a v0.10.0.

### INSTALAÇÃO ###

npm install -g protractor   (para instalar globalmente)
protractor --version (para verificar a versão instalada)
npm install -g webdriver-manager

**Instalação do Webdriver e do Protractor como depedência do projeto**

npm install protractor --save-dev
npm install webdriver-manager --save-dev

**Atualização do webdriver-manager**
webdriver-manager update

### ESTRUTURA DE PASTAS E ARQUIVO DE CONFIGURAÇÃO ###

Dentro da estrutura do projeto criar uma pasta chamada "tests" e dentro dessa pasta uma pastas chamada "e2e";
Na pasta e2e, criar um arquivo chamado protractor.conf.js

Dentro do arquivo protractor.conf.js:

// protractor.conf.js

 module.exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    capabilities: {
    'browserName': 'chrome'
    },
    specs: ['specs/*.spec.js'],
    baseUrl: 'http://www.tokenlab.com.br/pt/'
};


### CRIANDO O PRIMEIRO TESTE ###

Dentro da pasta e2e, criar um novo subdiretório chamado "specs".
Neste diretório, criar um arquivo chamado "contato.spec.js"


### RODANDO OS TESTES ###

Em uma aba do terminal inicie o webdriver:

webdriver-manager start

em outra aba, no diretorio tests/e2e execute o comando 
protractor

