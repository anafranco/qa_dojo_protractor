//contato.spec.js

var Contato = require('../page-objects/contato.po.js');

describe('Suite 1: Formulário de contato', function() {

    var contato = new Contato();


    it('CT1: Sucesso no envio do formulário', function() {

        //Arrange (pré-condições)
        contato.visit();

        //Act (passos)
        contato.preencheContatoForm('Ana', 'anafranco@tokenlab.com.br', 'Empresa Teste', '[ignore-inbox] Assunto Teste', 'Mensagem Teste');

        //Asserts (resultado esperado)
        expect(contato.textForm.getText()).toEqual('Mensagem recebida! Entraremos em contato em breve. Obrigado!'); 
    });
});