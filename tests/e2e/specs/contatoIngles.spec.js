var Contato = require('../page-objects/contato.po.js');

describe("Suite 2: Formulario de contato em inglês",function(){
    var contato = new Contato()

    it("CT1: Sucesso ao enviar o formulario em inglês", function(){
        contato.visit()
        contato.changeLanguage()
        contato.preencheContatoForm('Anne', 'annefrankie@tokenlab.com', 'Test Company', '[ignore-inbox] Test Subject', 'Test Message');
        browser.sleep(5000);
        
        expect(contato.textForm.getText()).toEqual("Message received! We'll get back to you soon. Thanks!")
    })

})