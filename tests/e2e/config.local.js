//config.local.js

/**
 * Configuration alter method.
 * @param {object} Current Protractor configuration, as defined in
 * protractor.conf.js file
 */

module.exports = function (config) {
    config.baseUrl = 'http://localhost:8000/';
    config.directConnect = true;
};

