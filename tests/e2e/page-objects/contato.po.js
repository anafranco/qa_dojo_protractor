//contato.po.js

var Contato = function () {
    this.nameField = element(by.id('contact_name'));
    this.emailField = element(by.id('contact_email'));
    this.companyField = element(by.id('contact_company'));
    this.subjectField = element(by.id('contact_subject'));
    this.messageField = element(by.id('contact_message'));
    this.sendButton = element(by.css('.btn-primary'));

    this.textForm = element(by.id('msg_envio'));

    this.flag = element.all(by.css(".lang-switch")).last();
    this.englishText = element.all(by.css(".flag")).last();

};

Contato.prototype.preencheContatoForm = function(name, email, company, subject, message){

    this.nameField.sendKeys(name);
    this.emailField.sendKeys(email);
    this.companyField.sendKeys(company);
    this.subjectField.sendKeys(subject);
    this.messageField.sendKeys(message);

    this.sendButton.click();
}

Contato.prototype.visit = function() {
    browser.get('/contacts/');
}

Contato.prototype.changeLanguage = function(){

    this.flag.click();
    this.englishText.click();
}

module.exports = Contato;