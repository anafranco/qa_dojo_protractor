// protractor.conf.js

var fs = require('fs');

module.exports.config = {

    directConnect: true,

    capabilities: {
    'browserName': 'chrome',
    },

    specs: ['specs/*.spec.js'],
    baseUrl: 'http://www.tokenlab.com.br/pt',
    framework: 'jasmine2'
};

  //Local config customization
  try {
    fs.statSync(_dirname + '/config.local.js');
    require(_dirname + '/config.local.js')(module.exports.config);
} catch(error) {
    return;
}